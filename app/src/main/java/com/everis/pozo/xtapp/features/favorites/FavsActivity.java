package com.everis.pozo.xtapp.features.favorites;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.everis.pozo.xtapp.R;
import com.everis.pozo.xtapp.SQLite.SQLiteFavs;
import com.everis.pozo.xtapp.product.Product;
import com.everis.pozo.xtapp.utils.Utils;

import java.util.List;

public class FavsActivity extends AppCompatActivity implements View.OnClickListener {
   
    private RecyclerView recyclerView;
    private  FavsAdapter adapter;
    List<Product> products;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_favs);

            //SE INSTANCIA LA BASE DE DATOS
            SQLiteFavs sqLiteFavs = SQLiteFavs.getInstance(this);

            //SE OTIENE LA LISTA DE PRODUCTOS DESDE UN METODO DE LA BASE DE DATOS
            products = sqLiteFavs.getAllProduct();
            adapter = new FavsAdapter(products, this);

        if (products.size() == 0) {
            findViewById(R.id.tvSinFavs).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tvSinFavs).setVisibility(View.GONE);
        }

            recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

            //SE INSERTA ACTIONBAR Y BOTON ATRAS
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                setSupportActionBar(toolbar);
                getSupportActionBar().setTitle(R.string.favsTitle);
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
        }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ivDelete:
                Product product = (Product) v.getTag();
                SQLiteFavs sqLiteFavs = SQLiteFavs.getInstance(this);
                int d = sqLiteFavs.deleteProduct(product.getId());

                if(d > 0){
                    products.remove(product);
                    Toast toast = Toast.makeText(this, "Oferta eliminada", Toast.LENGTH_LONG);
                    toast.show();
                }
                adapter.notifyDataSetChanged();
                break;
            case R.id.ivBuy:
                Product product1 = (Product) v.getTag();
                Utils.presentWebView(this, product1.getPagUrl(), product1.getCompany(), product1);
                adapter.notifyDataSetChanged();
                break;
        }
    }
}