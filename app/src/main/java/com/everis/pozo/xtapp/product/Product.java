package com.everis.pozo.xtapp.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Product implements Serializable {
//PARA PODER MANDAR OBJETOS A OTRA ACTIVIDAD SE IMPLEMENTA COMO CLASE SERIALIZABLE

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("company")
    @Expose
    private String company;

    @SerializedName("urlImage")
    @Expose
    private String imageUrl;

    @SerializedName("urlOfficial")
    @Expose
    private String pagUrl;

    @SerializedName("model")
    @Expose
    private String model;

    @SerializedName("price")
    @Expose
    private String price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPagUrl() {
        return pagUrl;
    }

    public void setPagUrl(String pagUrl) {
        this.pagUrl = pagUrl;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}