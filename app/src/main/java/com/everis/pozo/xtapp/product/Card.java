package com.everis.pozo.xtapp.product;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.everis.pozo.xtapp.R;

import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;

@Layout(R.layout.card_view)
public class Card {

    @View(R.id.productImageView)
    private ImageView productImageView;

    @View(R.id.companyModelTxt)
    private TextView companyModelTxt;

    @View(R.id.priceTxt)
    
    private TextView priceTxt;
    private Product mProduct;
    private Context mContext;
    private OnFinishSwipeCard onFinishSwipeCard;

    public Card (Context context, Product product, OnFinishSwipeCard onFinishSwipeCard) {
        mContext = context;
        mProduct = product;
        this.onFinishSwipeCard = onFinishSwipeCard;
    }

    @Resolve
    private void onResolved(){
        Glide.with(mContext).load(mProduct.getImageUrl()).into(productImageView);
        companyModelTxt.setText(mProduct.getCompany() + ": " + mProduct.getModel());
        priceTxt.setText("Precio: " + mProduct.getPrice() + " €");
    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
        //navego a anterior
        if(onFinishSwipeCard != null)
            onFinishSwipeCard.finish(false, mProduct);
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn() {
        Log.d("EVENT", "onSwipedIn");
        //navego a la web
        if(onFinishSwipeCard != null)
            onFinishSwipeCard.finish(true, mProduct);
    }

    @SwipeInState
    private void onSwipeInState(){
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState(){
        Log.d("EVENT", "onSwipeOutState");
    }

    interface OnFinishSwipeCard{
        void finish(boolean buyed, Product product);
    }
}