package com.everis.pozo.xtapp.features.captureaudio;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;

import com.everis.pozo.xtapp.product.ProductActivity;
import com.everis.pozo.xtapp.R;
import com.everis.pozo.xtapp.features.home.HomeActivity;

import com.gabrielmorenoibarra.g.G;
import com.qraider.xt.XTUltrasonicsActivity;

import com.rodolfonavalon.shaperipplelibrary.ShapeRipple;
import com.rodolfonavalon.shaperipplelibrary.model.Circle;

import java.util.Timer;
import java.util.TimerTask;

public class ListeningActivity extends XTUltrasonicsActivity {

    private ShapeRipple ripple;
    private int mProximityLevel = 0;
    private ProximityReset mProximityResetTimer;
    private Timer mProximityIndicatorTimer;
    private ImageView backButton;
    private View v;
    private int green = 5;
    private int red = 4;
    private int orange = 3;
    private int yellow = 2;
    private int blue = 1;
    private int gray = 0;
    private String prePermissionTitle = "Atención!";
    private String prePermissionBody = "Esta aplicación detecta AudioBeacons. Para usar esta aplicación, necesitamos acceder a tu micro.";
    private String accessMicTitle = "Acceso al micro denegado!";
    private String accessMicBody = "Esta aplicación detecta AudioBeacons. Para usar esta aplicación, necesitamos acceder a tu micro. Por favor, concede el acceso al micro en ajustes.";


    //DDURACION DEFINIDA POR EL DESARROLLADOR
    private static final int DURATION = 1500;
    private double amplitude = 0;
    private static final int notiDURATION = 500;

    MediaPlayer mp;

    //METODOS PARA LA DETECCION
    @Override
    public void didHearTriggerWithTitle(String title, double amplitude) {
        super.didHearTriggerWithTitle(title, amplitude);

        //SI DETECTA EL LOOP NOS MUESTRA DIFERENTES COLORES DEPENDIENDO DE LA CERCANIA AL EMISOR
        if (title.equalsIgnoreCase("C-400-399") || title.equalsIgnoreCase("C-399-400")) {

            //ESTABLE EL NIVEL DE PROXIMIDAD BASADO EN LA AMPLITUD DE ULTRASONIDO
            if (amplitude > 200000) {
                mProximityLevel = red;
            } else if (amplitude > 100000) {
                mProximityLevel = orange;
            } else if (amplitude > 20000) {
                mProximityLevel = yellow;
            } else if (amplitude > 0) {
                mProximityLevel = blue;
            } else {
                mProximityLevel = gray;
            }

            //CADA DOS SEGUNDOS SE REFRESCA Y BAJA UN NIVEL DE PROXIMIDAD SI EL ULTRASONIDO NO SE RECIBE
            if (mProximityResetTimer != null) {
                mProximityResetTimer.stop = true;
                mProximityResetTimer = null;
            }
            mProximityResetTimer = new ProximityReset();
        }

        // SEGUN EL AUDIO DETECTADO:
        if (title != null) {

            switch (title) {

                case "C-96-399":
                    mProximityLevel = green;
                    break;
                case "C-96-400":    //SPOT APPLE
                    mProximityLevel = green;
                    alert(v, 1);
                    break;
                case "C-100-399":
                    mProximityLevel = green;
                    break;
                case "C-100-400":   //SPOT RAYBAN
                    mProximityLevel = green;
                    alert(v, 0);
                    break;
                case "C-397-399":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-399-100":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-399-397":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-399-400":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-400-95":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-400-96":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-400-100":
                    //Establece proximidad en verde para indicar detección
                    mProximityLevel = green;
                    break;
                case "C-400-399":
                    //Establece proximidad en verde para indicar detección
                   mProximityLevel = green;
                    break;
                default:
                    break;
            }
        }
    }

    //AVISOS PARA ACEPTAR PERMISOS (if handleRecordPermissionsForMe = true)
    void setDialogTexts() {
        kPrePermissionTitle = prePermissionTitle;
        kPrePermissionBody = prePermissionBody;
        kPermissionBodyRejected = prePermissionBody;
        kMicPermissionDenyTitle = accessMicTitle;
        kMicPermissionDenyBody = accessMicBody;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening);

        //PERMISOS
        handleRecordPermissionsForMe = true;

        //LLAMA A LOS AVISOS
        setDialogTexts();

        //INDICADOR DE PROXIMIDAD, ANIMACIÓN
        ripple = (ShapeRipple) findViewById(R.id.proximitiIndicator);
        setUpSoundBeaconUI();

        //BOTON ATRAS
        backButton = (ImageView) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v1) {
                //LLAMA AL METEDO PARA NAVEGAR A ATRAS
                cancel(v1);
            }
        });
        //SE SOMBREA EL BOTON ATRAS CUANDO SE PRESIONA
        G.setAlphaSelector(backButton, backButton);
    }

    @Override
    public void onResume() {
        super.onResume();
        mProximityLevel = gray;
    }

    //MODIFICACION INDICADOR UI
    private void setUpSoundBeaconUI() {
        if (ripple == null) return;

        ripple.setRippleShape(new Circle());
        ripple.setEnableSingleRipple(false);
        ripple.setRippleDuration(DURATION);
        mProximityIndicatorTimer = new Timer();
        mProximityIndicatorTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                indicateProximity();
            }
        }, gray, DURATION);
    }

    private void resetProximity() {
        if (mProximityLevel == green) return;

        //NIVEL DE PROXIMIDAD MAS BAJO
        if (mProximityLevel > gray)
            --mProximityLevel;
    }

    //MODIFICA EL COLOR DEPENDIENDO DE LA PROXIMDAD
    private void indicateProximity() {
        if (ripple == null) return;

        switch (mProximityLevel) {
            case 5:
                //color: verde
                ripple.setRippleColor(ContextCompat.getColor(ListeningActivity.this, R.color.green));
                break;
            case 4:
                //color: rojo
                ripple.setRippleColor(ContextCompat.getColor(ListeningActivity.this, R.color.red));
                break;
            case 3:
                //color: naranja
                ripple.setRippleColor(ContextCompat.getColor(ListeningActivity.this, R.color.orange));
                break;
            case 2:
                //color: amarillo
                ripple.setRippleColor(ContextCompat.getColor(ListeningActivity.this, R.color.yellow));
                break;
            case 1:
                //color: azul
                ripple.setRippleColor(ContextCompat.getColor(ListeningActivity.this, R.color.blue));
                break;

            default:
                //color: gris
                ripple.setRippleColor(ContextCompat.getColor(ListeningActivity.this, R.color.gray));
                break;
        }
    }

    public class ProximityReset implements Runnable {
        private Handler handler;
        public boolean stop;

        public ProximityReset () {
            handler = new Handler(Looper.getMainLooper());
            loop();
        }

        @Override
        public void run() {
            if (stop){
                handler.removeCallbacks(this);
                return;
            }
            resetProximity();
            loop();
        }

        private void loop() {
            handler.postDelayed(this, DURATION);
        }
    }

    //NAVEGACION A MAIN
    public void cancel(View button) {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    //NAGEVA A LA VISTA PRESENTACION DEL PRODUCTO
    public void alert(View v1, int pos) {
        //vibracion al mostrar el producto
        Vibrator vibs = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibs.vibrate(notiDURATION);

        //ALERTA SONORA AL MOSTRAR EL PRODUCTO
        mp = MediaPlayer.create(this, R.raw.twinkle);
        mp.setVolume(0.025f,0.025f);
        mp.start();

        Intent intent = new Intent(this, ProductActivity.class);

        intent.putExtra("pos", pos);    //manda la posicion del array productos
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);   // Vuelve directamente a main
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out); //efecto entre vistas
    }
}