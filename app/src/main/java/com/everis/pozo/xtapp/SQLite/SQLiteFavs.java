package com.everis.pozo.xtapp.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.everis.pozo.xtapp.product.Product;

import java.util.ArrayList;
import java.util.List;

public class SQLiteFavs extends SQLiteOpenHelper {

    private static final String NOMBRE = "productsdb";
    private static final int VERSION = 1;

    private static SQLiteFavs sInstance;

    public SQLiteFavs(Context context) {
        super(context, NOMBRE, null, VERSION);
    }

    public static synchronized SQLiteFavs getInstance(Context context) {

        if (sInstance == null) {
            sInstance = new SQLiteFavs(context.getApplicationContext());
        }
        return sInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //SE CREA LA TABAL FAVS(ID, URLIMAGE, URLOFFICIAL, COMPANY, MODEL, PRICE)
        db.execSQL("create table favs(id integer primary key unique, urlImage text, urlOfficial text, company text, model text, price text)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int version1, int version2) {
        db.execSQL("drop table if exists favs");
        db.execSQL("create table favs(id integer primary key unique, urlImage text, urlOfficial text, company text, model text, price text)");
    }

    public List<Product> getAllProduct(){

        //SE OBTIENE UNA INSTACIA DE LA BASE DE DATOS A MODO LECTURA
        SQLiteDatabase db = getReadableDatabase();

        //SE OBTIENE UN CURSOR MEDIANTE UN QUERY A LA BASE DE DATOS db
        Cursor cursor = db.rawQuery("SELECT * FROM favs", null);

        //SE CREA LA LISTA DE PRODUCTS
        List<Product> products = new ArrayList<>();

        try {
            //SE UBICA EL CURSOR EN LA PRIMERA POSICION
            if (cursor.moveToFirst()) {
                do {

                    //SE INSTANCIA EL PRODUCT Y LO RELLENAMOS CON LOS DATOS DE CADA ITERACIÓN
                    Product product = new Product();
                    product.setId(cursor.getInt(cursor.getColumnIndex("id")));
                    product.setCompany(cursor.getString(cursor.getColumnIndex("company")));
                    product.setImageUrl(cursor.getString(cursor.getColumnIndex("urlImage")));
                    product.setModel(cursor.getString(cursor.getColumnIndex("model")));
                    product.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    product.setPagUrl(cursor.getString(cursor.getColumnIndex("urlOfficial")));
                    products.add(product);

                    //SE DESPLAZA EL CURSO A LA SIGUIENTE POSICION HASTA QUE FINALICE
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            //PARA FINALIZAR SE CIERRA LA BASE DE DATOS bd
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return products;
    }
    //METODO QUE AÑADE PRODUCTO A LA BASE DE DATOS
    public long addProduct(Product product) {

        long id = -1;

        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {

            ContentValues values = new ContentValues();
            values.put("company", product.getCompany());
            values.put("urlImage", product.getImageUrl());
            values.put("urlOfficial", product.getPagUrl());
            values.put("model", product.getModel());
            values.put("price", product.getPrice());
            values.put("id", product.getId());

            id = db.insertOrThrow("favs", null, values);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        return id;
    }

    //METODO QUE ELIMINA EL PRODUCTO QUE EL USUARIO DECIDE
    public int deleteProduct(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rows = db.delete("favs", "id=id", null);
        db.close();

        return rows;
    }
}