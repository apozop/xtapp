package com.everis.pozo.xtapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.util.Log;

import com.everis.pozo.xtapp.features.buy.WebViewActivity;
import com.everis.pozo.xtapp.product.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static final String TAG = "Utils";

    public static List<Product> loadProducts(Context context){
        try{
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            JSONArray array = new JSONArray(loadJSONFromAsset(context, "products.json"));
            List<Product> productList = new ArrayList<>();
            for(int i=0;i<array.length();i++){
                Product product = gson.fromJson(array.getString(i), Product.class);
                productList.add(product);
            }
            return productList;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json = null;
        InputStream is=null;

        try {
            AssetManager manager = context.getAssets();
            Log.d(TAG,"path "+jsonFileName);
            is = manager.open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    //METODO UBICAQO EN UTILS PARA REUTILIZARLO DESDE LA CARD DEL MOMENTO DE LA DETECCION Y LA VISTA DE MIS FAVORITOS
    public static void presentWebView(Activity activity, String URL, String company, Product product) {
        Intent i = new Intent(activity, WebViewActivity.class );
        i.putExtra(WebViewActivity.EXTRA_TITLE, company);
        i.putExtra(WebViewActivity.EXTRA_URL, URL);
        i.putExtra("PRODUCT", product);
        activity.startActivity(i);
    }
}