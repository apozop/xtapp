package com.everis.pozo.xtapp.product;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.everis.pozo.xtapp.R;
import com.everis.pozo.xtapp.utils.Utils;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.listeners.ItemRemovedListener;

import java.util.List;

public class ProductActivity extends AppCompatActivity implements Card.OnFinishSwipeCard, View.OnClickListener {

    private SwipePlaceHolderView mSwipeView;
    private int numCards = 1;
    private int padCards = 20;
    private float scaleCards = 1.01f;
    private ImageButton acceptBtn;
    Product product = null ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        mSwipeView = (SwipePlaceHolderView)findViewById(R.id.swipeView);
        acceptBtn = (ImageButton) findViewById(R.id.acceptBtn);
        acceptBtn.setOnClickListener(this);

        //SE OBTIENE LA POSICION DEL OBJETO
        int pos = getIntent().getIntExtra("pos",0);

        //CARGA PRODUCTOS
        List<Product> products = Utils.loadProducts(ProductActivity.this);

        //SE CHEQUEAN LOS PRODUCTOS OBTENIDOS
        if(products != null && products.size() > 0){

            product = products.get(pos);
            mSwipeView.getBuilder()
                    .setDisplayViewCount(numCards)
                    .setSwipeDecor(new SwipeDecor()
                            .setPaddingTop(padCards)
                            .setRelativeScale(scaleCards)
                            .setSwipeInMsgLayoutId(R.layout.swipe_in_view)
                            .setSwipeOutMsgLayoutId(R.layout.swipe_out_view));
            mSwipeView.addItemRemoveListener(new ItemRemovedListener() {
                @Override
                public void onItemRemoved(int count) {
                    Log.i("","");
                }
            });
            mSwipeView.addView(new Card(ProductActivity.this, product, this));
            findViewById(R.id.rejectBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSwipeView.doSwipe(false);
                }
            });
            findViewById(R.id.acceptBtn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSwipeView.doSwipe(true);
                }
            });
        } else {
            //ERROR: NO HAY PRODUCTOS EN EL JSON
        }
    }

        //ESTE METODO RECUPERA LA MARCA Y LA URL DE LA PAGINA OFICIAL Y LA MANDA A TRABES DE PRESENTWEBVIEW()
        public void toBuy(Boolean compra, Product product) {
            if (compra) {
                Utils.presentWebView(this, product.getPagUrl(), product.getCompany(), product);
            }
            else {
                finish();
            }
        }

    @Override
    public void finish(boolean buyed, Product product) {
        toBuy(buyed, product);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.acceptBtn:
                break;
            case R.id.rejectBtn:
                break;
        }
    }
}