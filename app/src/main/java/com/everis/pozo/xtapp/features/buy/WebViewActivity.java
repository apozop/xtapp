package com.everis.pozo.xtapp.features.buy;

import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.everis.pozo.xtapp.R;
import com.everis.pozo.xtapp.SQLite.SQLiteFavs;
import com.everis.pozo.xtapp.product.Product;

import java.io.Serializable;

public class WebViewActivity extends AppCompatActivity {
    private WebView mWebView;
    public static final String EXTRA_URL = "com.everis.pozo.Webviews.url";
    public static final String EXTRA_TITLE = "com.everis.pozo.Webviews.title";
    public static final Serializable PRODUCT = null;
    private String mURL;
    private String mTitle;
    private Product mProduct;
    private ProgressBar progressBar;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mURL = getIntent().getStringExtra(EXTRA_URL);
        mTitle = getIntent().getStringExtra(EXTRA_TITLE);
        setContentView(R.layout.web_display);

        //SE INSERTA LA ACTIONBAR Y EL BOTÓN ATRÁS
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(mTitle);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        mWebView = (WebView)findViewById(R.id.webView);

        if(mWebView != null){
            WebSettings settings = mWebView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setDomStorageEnabled(true);
            mWebView.loadUrl(mURL);
            mWebView.setWebViewClient(new MyWebViewClient());
        }

        //BARRA DE PROGRESO
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax(10);
        new Thread(new Runnable() {
            public void run() {

                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setVisibility(View.VISIBLE);
                            progressBar.setProgress(0);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
            }
        }).start();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            //Ignorará el error de Ssl y seguirá adelante
            handler.proceed();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            onStop();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    if(mWebView.canGoBack()){
                        mWebView.goBack();
                    }else{
                        finish();
                    }
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    //AÑADIR A ACTION BAR BOTON AÑADIR A FAVORITOS:
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Utilizar los elementos del menu en la barra de acción
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.fav_button, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //AGREGAR RESPUESTA AL BOTON AÑADIR A FAVORITOS
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Al presion los elementos de la barra de acción
        switch (item.getItemId()) {
            case R.id.action_fav:
                addFav();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //METODO QUE AÑADE OBJETOS PRODUCTO A FAVORITOS
    public void addFav(){

        mProduct = (Product) getIntent().getSerializableExtra("PRODUCT");
        SQLiteFavs favs = SQLiteFavs.getInstance(this);
        long id = favs.addProduct(mProduct);

        if(id == -1){
            //YA ESTA AÑADIDA A LA BASE DE DATOS db
            Toast toast = Toast.makeText(this, R.string.favYaSeAdd, Toast.LENGTH_LONG);
            toast.show();
        } else {
            //CORRECTAMENTE AÑADIDO
            Toast toast = Toast.makeText(this, R.string.favAdd, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    //OCULTA LA BARRA DE PROGRESO
    @Override
    public void onStop() {
        super.onStop(); // super para el menú
        progressBar.setVisibility(View.INVISIBLE);    //GONE
    }

}