package com.everis.pozo.xtapp.features.captureaudio;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.media.MediaRecorder;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureAudioService extends Service {
    //TODO: SERVICIO PARA REALIZAR ESCUCHA EN SEGUNDO PLANO EN DESARROLLO

    private String TAG = "CaptureAudioService";
    private static final int min_free_storagespace = 10;
    private static final double amplitude_threshold = 200;
    private static final int number_of_retries = 60;
    private static final long service_runnable_interval = 1000;
    private Handler mhandler;
    private Runnable mrunnable;
    private MediaRecorder _recorder;
    private String path;
    private String current_filename;
    private boolean hasRecorded;
    private int attempt;
    private int serverResponseCode = 0;
    private String upLoadServerUri;
    private File[] uploadFiles;


    //ENLAZA EL SERVICIO CON LOS METODOS QUE SON NECESARIOS PARA SU USO
    private final IBinder mBinder = new LocalBinder();
    private final ListeningActivity mAudio = new ListeningActivity();

    @SuppressLint("HardwareIds")
    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "CaptureAudioService Created");

        //SE CREA EL SERVICIO
        mhandler = new Handler();

        _recorder = new MediaRecorder();

        if (hasRecorded=true){

        }
    }

    //TAREA EN SEGUNDO PLANO
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "CaptureAudioService onStartCommand");

        start_new_recording();


        mrunnable = new Runnable() {
            public void run() {

                    // Returns the maximum absolute amplitude that was sampled since the last call to this method.
                    int amplitude = _recorder.getMaxAmplitude();
                    Log.d(TAG, "Amplitude: " + amplitude);
                    Log.d(TAG, "Attempt: " + attempt);
                    if (amplitude > amplitude_threshold) {
                        // The amplitude of the last measurement was higher than the threshold.
                        hasRecorded = true;
                        attempt = 0;
                        // Continue recording.
                        Log.d(TAG, "Continue recording.");
                    } else {
                        // The amplitude of the last measurement wasn't higher than the threshold.
                        if (attempt >= number_of_retries) {
                            // No more attempts left, stop the MediaRecorder.
                            Log.d(TAG, "Stop recording.");
                        //    _recorder.stop();
                            if (hasRecorded) {
                                // Something with a high enough amplitude has been recorded, start uploading.
                              //  uploadAudioFiles();
                            } /*else {
                                // Nothing interesting has been recorded, thus delete the file.
                                delete_file(current_filename);
                            }*/
                            _recorder.reset();
                            // Restart the MediaRecorder.
                            start_new_recording();
                        } else {
                            // There are some attempts left, update the counter and continue.
                            attempt++;
                        }
                    //}
                }
                mhandler.postDelayed(mrunnable, service_runnable_interval);
            }
        };
        mhandler.postDelayed(mrunnable, service_runnable_interval);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.
        return START_STICKY;
    }

    private void configure_recorder() {
        Log.d(TAG, "Configuring the MediaRecorder");
        // Sets the audio source to be used for recording.
        _recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        // Sets the format of the output file produced during recording.
        _recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        // Sets the audio encoder to be used for recording.
        _recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        // Sets the audio sampling rate for recording.
        _recorder.setAudioSamplingRate(44100);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        Date now = new Date();
        current_filename = formatter.format(now);
        // Sets the path of the output file to be produced.
       _recorder.setOutputFile(getCacheDir().getAbsolutePath() + File.separator + current_filename + "3gp");

        // Prepares the recorder to begin capturing and encoding data.
        try {
            _recorder.prepare();
        } catch (IOException e) {
            Log.e(TAG, "prepare() failed");
        }
    }

    /**
     * @brief Configure the MediaRecorder and start a new recording.
     */
    private void start_new_recording() {
        Log.d(TAG, "Starting a new recording.");
        configure_recorder();
        hasRecorded = false;
        attempt = 0;
        _recorder.start();
    }

    /**
     * @param _message The message to show.
     * @brief Makes a notification for the user
     */
    public void notifyUser(String _message) {

    }

    /**
     * @brief Handles when the Service stops.
     */
    @Override
    public void onDestroy() {
        Log.d(TAG, "CaptureAudioService Destroyed");
        ClearLED();
        super.onDestroy();
        _recorder.reset();
        _recorder.release();
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        CaptureAudioService getService() {
            // Return this instance of LocalService so clients can call public methods
            return CaptureAudioService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    /** method for clients */
    public ListeningActivity getAudio() {
        return mAudio;
    }

    /**
     * @brief Lets the LED indicator flash.
     */
    private void FlashLED() {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification notif = new Notification();
        notif.ledARGB = 0xFF0000ff;
        notif.flags = Notification.FLAG_SHOW_LIGHTS;
        notif.ledOnMS = 100;
        notif.ledOffMS = 900;
        nm.notify(0, notif);
    }

    /**
     * @brief Clears the LED indicator.
     */
    private void ClearLED() {
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancel(0);
    }

    /**
     * @return true  Enough. false   Too little.
     * @brief Determines if there is enough storage space.
     */
    public boolean IsThereStorageSpace() {
        // Is the amount of free space <= min_free_storagespace% of the total space, return false. Otherwise return true;
        long freespace = Environment.getExternalStorageDirectory().getFreeSpace() / 1048576;
        long totalspace = Environment.getExternalStorageDirectory().getTotalSpace() / 1048576;
        Log.d(TAG, "StorageSpace: " + String.valueOf((int) (((double) freespace / (double) totalspace) * 100)) + "% remaining.");
        return freespace > ((totalspace / 100) * min_free_storagespace);
    }

    /**
     * @return True/false.
     * @brief Checks if external storage is available for read and write.
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

}