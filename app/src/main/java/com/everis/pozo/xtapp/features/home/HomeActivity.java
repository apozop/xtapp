package com.everis.pozo.xtapp.features.home;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.everis.pozo.xtapp.R;
import com.everis.pozo.xtapp.features.captureaudio.CaptureAudioService;
import com.everis.pozo.xtapp.features.captureaudio.ListeningActivity;
import com.everis.pozo.xtapp.features.favorites.FavsActivity;

import com.gabrielmorenoibarra.g.G;

import io.fabric.sdk.android.Fabric;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout mDrawerLayout;
    ImageView micOffButton;
    int notificationID = 1;
    boolean auto;
    boolean status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        //SE OBTIENE UNA REFERENCIA PARA LOS CONTROLES DE LA INTERFAZ
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ImageView settingsButton = (ImageView) findViewById(R.id.settingsButton);

        //EVENTO BOTON AJUSTES
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });

        //SE SOMBREA EL ICONO AJUSTES CUANDO SE PRESIONA
        G.setAlphaSelector(settingsButton, settingsButton);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //TODO:EVENTO BOTON MICROFONO DESHABILITAO POR ESTAR EN DESARROLLO
        micOffButton = (ImageView) findViewById(R.id.micOffButton);
       /* micOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                status = (boolean) v.getTag();
                autoMode(!status);
            }
        });*/

        //SE SOMBREA EL ICONO DEL MICROFONO MODO AUTO CUANDO SE PRESIONA
        G.setAlphaSelector(micOffButton, micOffButton);

        //EVENTO LINK MIS FAVORITOS
        TextView favs = (TextView) findViewById(R.id.favs);
        G.setAlphaSelector(favs);
        favs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("tag","entra a Favs");
                myfavs(v);
            }
        });

        //EVENTO BOTON ESCUCHAR
        ImageView hearButton = (ImageView) findViewById(R.id.hearButton);
        G.setAlphaSelector(hearButton);
        hearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("tag","click a aro");
                hear(v);
            }
        });

        //ANIMACIÓN BOTON ESCUCHAR
        Animation anim_hoop = AnimationUtils.loadAnimation(this, R.anim.anim_hoop);
        hearButton.startAnimation(anim_hoop);
        anim_hoop.setRepeatMode(ScaleAnimation.INFINITE);

        //TODO: PREFERENCIA DEL USUARIO PARA EL MODO AUTO EN DESARROLLO
        SharedPreferences preferences = getSharedPreferences("MyPreferences",Context.MODE_PRIVATE);
        auto = preferences.getBoolean("MODO_AUTO", false);
        micOffButton.setTag(auto);

        //si true llamo al auto,
        if(auto){
            autoMode(status);
        }

    }

    public void myfavs(View v) {
        Intent intent = new Intent(this, FavsActivity.class);
        startActivity(intent);
    }

    //ACTIVA O DESACTIVA MODO AUTO
    public void autoMode(boolean auto) {

        //PREFERENCIAS DEL USUARIO
        SharedPreferences preferences = getSharedPreferences("MyPreferences",Context.MODE_PRIVATE);

        if (auto) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(HomeActivity.this, R.color.colorAccent));
            }
            micOffButton.setImageResource(R.mipmap.ic_mic_on);
            micOffButton.setTag(true);
            Switch switchAB = (Switch)findViewById(R.id.switchAB);
            switchAB.setChecked(true);

            //MODIFICACION DE PREFERECIAS
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("MODO_AUTO", true);
            editor.apply();
            notificationsBuilder();
            startService(new Intent(HomeActivity.this, CaptureAudioService.class));
        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setStatusBarColor(ContextCompat.getColor(HomeActivity.this, R.color.colorPrimaryDark));
            }
            micOffButton.setImageResource(R.mipmap.ic_mic_off);
            micOffButton.setTag(false);
            Switch switchAB = (Switch)findViewById(R.id.switchAB);
            switchAB.setChecked(false);

            //MODIFICACION DE PREFERECIAS
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("MODO_AUTO", false);
            editor.apply();

            stopService(new Intent(HomeActivity.this, CaptureAudioService.class));

            //ELEMINA LA NOTIDFICACION AL DESACTIVAR EL MODO AUTO
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            nm.cancelAll();
        }
    }

    //TODO: SE DETECTA QUE ITEM DEL MENU SE ELIGE, EN DESARROLLO
    public boolean onNavigationItemSelected(MenuItem item) {
        // Click en el ítem de la vista de navegacion
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_auto:
                break;
            case R.id.nav_noti:
                break;
            case R.id.nav_speed:
                break;
            case R.id.nav_vibrar:
                break;
            default: break;
        }
        return true;
    }

    /* BOTON RETROCESO DE NAVIGATIONBAR CIERRA MENU */
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //BOTON ESCUCHAR
    public void hear(View v1) {
        Intent intent = new Intent(this, ListeningActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);           // Vuelve directamente a main.
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }

    //NOTIFICACION AL ACTIVAR MODO AUTO
    public void notificationsBuilder() {
            Intent i = new Intent(this, HomeActivity.class);
            i.putExtra("notificationID", notificationID);

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, 0);
            NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            CharSequence ticker = "XTapp detectará ofertas a tu alrededor";
            CharSequence contentTitle = "XTapp";
            CharSequence contentText = "Modo automático activado!";
            Notification noti = new Notification.Builder(this)
                    .setContentIntent(pendingIntent)
                    .setTicker(ticker)
                    .setContentTitle(contentTitle)
                    .setContentText(contentText)
                    .setSmallIcon(R.mipmap.ic_aro_amarillo)
                    .addAction(R.mipmap.ic_aro_amarillo, ticker, pendingIntent)
                    .setVibrate(new long[]{100, 250, 100, 500})
                    .build();

            nm.notify(notificationID, noti);
    }
}