package com.everis.pozo.xtapp.features.favorites;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.everis.pozo.xtapp.R;
import com.everis.pozo.xtapp.product.Product;

import com.gabrielmorenoibarra.g.G;

import java.util.List;


public final class FavsAdapter extends RecyclerView.Adapter<FavsAdapter.ViewHolder> {

    private final List<Product> items;
    private View.OnClickListener listener;

    public FavsAdapter(List<Product> items, View.OnClickListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_fav_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Product product = items.get(position);

        //SE MUESTRAN LOS CAMPOS DE LA TABLA DE CADA OBJETO PRODUCTO
        holder.tvCompany.setText(product.getCompany() + ": ");
        holder.tvModel.setText(product.getModel());
        holder.tvPrice.setText("Precio: " + product.getPrice() + "€");

        //CONSIGUE MOSTRAR UNA URL COMO UNA IMAGEN
        Glide.with(holder.ivProduct.getContext()).load(product.getImageUrl()).into(holder.ivProduct);

        //SE SOMBRE EL IV CUANDO SE PRESIONA
        G.setAlphaSelector(holder.ivBuy, holder.ivDelete);

        holder.ivDelete.setTag(product);
        holder.ivDelete.setOnClickListener(listener);
        holder.ivBuy.setTag(product);
        holder.ivBuy.setOnClickListener(listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvCompany;
        private TextView tvModel;
        private TextView tvPrice;
        private ImageView ivProduct;
        private ImageView ivBuy;
        private ImageView ivDelete;

        public ViewHolder(View itemView) {
            super(itemView);

            tvCompany = (TextView) itemView.findViewById(R.id.tvCompany);
            tvModel = (TextView) itemView.findViewById(R.id.tvModel);
            tvPrice = (TextView) itemView.findViewById(R.id.tvPrice);
            ivProduct = (ImageView) itemView.findViewById(R.id.ivProduct);
            ivBuy = (ImageView) itemView.findViewById(R.id.ivBuy);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
        }
    }
}