package com.everis.pozo.xtapp;

import android.test.AndroidTestCase;

import com.everis.pozo.xtapp.SQLite.SQLiteFavs;
import com.everis.pozo.xtapp.product.Product;

import java.util.List;

/**
 * Created by Pozo on 26/6/17.
 */

//TEST QUE SE SUELE HACER PARA COMPROBAR EL FUNCIONAMIENTO GENERAL TRAS IMPLEMENTAR FUNCIONALIDADES NUEVAS

public class SQLiteTest extends AndroidTestCase {


    public void testProductDBOK(){

        SQLiteFavs favs = SQLiteFavs.getInstance(getContext());
        assertTrue(favs != null);

        Product product = new Product();
        product.setModel("ergerger");
        product.setImageUrl("www.prueba.es");
        product.setCompany("Everis");
        product.setId(1);
        product.setPrice("300");

        long id = favs.addProduct(product);
        assertTrue(id != -1);

        List<Product> products = favs.getAllProduct();
        assertTrue(product != null && products.size() > 0);

    }
}
